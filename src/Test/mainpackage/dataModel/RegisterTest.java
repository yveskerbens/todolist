package mainpackage.dataModel;

import org.junit.Test;

import static org.junit.Assert.*;

public class RegisterTest {

    @Test
    public void createNewUser() {

        User user2 = new User("vivy22", "Nervelie1@wemlb.com",
                "Nervelie", "Marcelin", "1234568");
        Register.getInstance().createNewUser(user2);
    }

    @Test
    public void isMail() {
        assertTrue(Register.getInstance().isMail("yveskerbens@wemlb.com"));
    }

    @Test
    public void isUserFieldExist() {
        assertTrue( Register.getInstance().isUserFieldExist("yveskerbens"));
        assertTrue( Register.getInstance().isUserFieldExist("yveskerbens@wemlb.com"));
    }

    @Test
    public void saveNewUser() {
        User user1 = new User("yveskerbens", "yveskerbens@wemlb.com",
                "Yves Kerbens", "Declerus", "123456");
        Register.getInstance().saveNewUser(new UserAccount(user1));
    }

    @Test
    public void name() {
    }
}