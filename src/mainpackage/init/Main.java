package mainpackage.init;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import mainpackage.dataModel.Login;
import mainpackage.dataModel.UserAccount;

import java.io.IOException;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("../viewModel/signIn.fxml"));
        primaryStage.setTitle("Sign in");
        primaryStage.setScene(new Scene(root, 300, 400));
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void stop(){
        UserAccount userAccount = Login.getInstance().getUserAccount();
        //userAccount.saveUserData();
    }
}
