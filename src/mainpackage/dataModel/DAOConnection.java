package mainpackage.dataModel;

import java.sql.*;

public class DAOConnection {
    private static DAOConnection ourInstance = new DAOConnection();
    private final String DB_NAME = "TodoListDB.db";
    private final  String url = "jdbc:sqlite:./DBStorage/"+DB_NAME;
    private Connection connection;
    private PreparedStatement preparedStatement;

    public final String USER_TABLE = "Users";
    public final String USER_TABLE_FIRSTNAME_COLUMN = "firstname";
    public final String USER_TABLE_LASTNAME_COLUMN = "lastname";
    public final String USER_TABLE_EMAIL_COLUMN = "email";
    public final String USER_TABLE_USERNAME_COLUMN = "username";
    public final String USER_TABLE_PASSWORD_COLUMN = "password";

    public final String TODOLIST_TABLE = "TodoList";
    public final String TODOLIST_TABLE_USERNAME_COLUMN = "username";
    public final String TODOLIST_TABLE_SHORTDESCRIPTION_COLUMN = "shortDecription";
    public final String TODOLIST_TABLE_DETAILS_COLUMN = "details";
    public final String TODOLIST_TABLE_DEADLINE_COLUMN = "deadline";


    private final String CREATE_USER_TABLE ="CREATE TABLE IF NOT EXISTS "+USER_TABLE+"("
            + USER_TABLE_FIRSTNAME_COLUMN + " varchar(50), " + USER_TABLE_LASTNAME_COLUMN
            + " varchar(50), " + USER_TABLE_USERNAME_COLUMN + " varchar(50), " + USER_TABLE_EMAIL_COLUMN
            + " varchar(50), " + USER_TABLE_PASSWORD_COLUMN + " varchar(50))";

    private final String CREATE_TODOLIST_TABLE = "CREATE TABLE IF NOT EXISTS "+TODOLIST_TABLE+"("
            + TODOLIST_TABLE_USERNAME_COLUMN + " varchar(50), " + TODOLIST_TABLE_SHORTDESCRIPTION_COLUMN
            + " varchar(50), " + TODOLIST_TABLE_DETAILS_COLUMN + " varchar(50), " + TODOLIST_TABLE_DEADLINE_COLUMN
            + " varchar(50))";

    private DAOConnection() {
        try {
            Class.forName("org.sql.jdbc");
        } catch (ClassNotFoundException e) {
            e.getCause();

        }
        this.createTable(CREATE_USER_TABLE);
        this.createTable(CREATE_TODOLIST_TABLE);
        System.out.println(CREATE_TODOLIST_TABLE);
    }

    public boolean open( ) {
        try{
            connection = DriverManager.getConnection(url);
            return true;
        } catch (SQLException e) {
            e.getErrorCode();
            e.printStackTrace();
            return false;
        }
    }

    public  void close() {
        try {
            connection.close();

        } catch (SQLException e) {
            e.getMessage();
        }
    }


    private void createTable(String statement)  {
        try {
            connection = DriverManager.getConnection(url);
            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.execute();
        } catch (SQLException e) {
            System.out.println("Fail to connect to DB" + e.getMessage());
        } finally {
             try {
                 preparedStatement.close();
                 connection.close();
             } catch (SQLException e) {
                 e.getMessage();
             }
        }

    }

    public static DAOConnection getInstance() {
        return ourInstance;
    }

    public Connection getConnection() {
        return connection;
    }
}
