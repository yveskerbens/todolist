package mainpackage.dataModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Login {

    private static UserAccount userAccount;
    private static Login ourInstance = new Login();
    private DAOConnection dbConnection= DAOConnection.getInstance();

    private Login() {
    }

    public static Login getInstance() {
        return ourInstance;
    }

    public boolean isLoginSuccess(String userName, String password) {
        StringBuilder sb = new StringBuilder();
        sb.append("select * from ");
        sb.append(dbConnection.USER_TABLE);
        sb.append(" where ");
        sb.append(dbConnection.TODOLIST_TABLE_USERNAME_COLUMN);
        sb.append(" = '");
        sb.append(userName);
        sb.append("'");
        if(dbConnection.open()) {
            try(Connection conn = dbConnection.getConnection();
                PreparedStatement prstmt = conn.prepareStatement(sb.toString());
                ResultSet result = prstmt.executeQuery()) {
                while(result.next()) {
                    String userEmail =  result.getString(dbConnection.USER_TABLE_EMAIL_COLUMN);
                    String userFirstName =  result.getString(dbConnection.USER_TABLE_FIRSTNAME_COLUMN);
                    String userLastName =  result.getString(dbConnection.USER_TABLE_LASTNAME_COLUMN);
                    String passwd =  result.getString(dbConnection.USER_TABLE_PASSWORD_COLUMN);
                    if(password.equals(passwd)) {
                        userAccount = new UserAccount(new User(userName, userEmail, userFirstName, userLastName, passwd));
                        return true;
                    } else {
                        return false;
                    }
                }
            } catch (SQLException e) {
                System.out.println("Not ok");
                e.printStackTrace();
                return false;
            }
        }
        dbConnection.close();


        return false;
    }

    public UserAccount getUserAccount() {
        return userAccount;
    }
}
