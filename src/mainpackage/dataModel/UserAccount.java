package mainpackage.dataModel;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;

public class UserAccount {
    private User user;
    private ObservableList userTodoList;
    private ObservableMap <String, ObservableList<TodoItem>> dataCon;


    public UserAccount(User user) {
        this.user = user;
        this.userTodoList =  TodoData.getInstance().getTodoItems();
        this.dataCon = TodoData.getInstance().getDataContainer();
    }

    public User getUser() {
        return user;
    }

    public ObservableList getUserTodoList() {
        return userTodoList;
    }

    public ObservableMap<String, ObservableList<TodoItem>> getDataCon() {
        return dataCon;
    }

    public void  addUserTodoItem(TodoItem item) {
        TodoData.getInstance().addNewTodoItem(item);
    }

    //Save user data
    public void saveUserData() {
        TodoData.getInstance().saveTodoItems(user.getUserName());
    }

    //Load user data
    public void loadUserTodoData () {
        TodoData.getInstance().loadTodoItems(user.getUserName());
    }

    //Delete user profile

    //Recover password

    //Modidy user data
}
