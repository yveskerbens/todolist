package mainpackage.dataModel;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;

import java.sql.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;



public class TodoData {


    private static TodoData instance = new TodoData();

    private  DateTimeFormatter dateTimeFormatter;

    private  DAOConnection dbInstance = DAOConnection.getInstance();

    public static TodoData getInstance() {
        return instance;
    }

    private ObservableList<TodoItem> todoItems = FXCollections.observableArrayList();


    private ObservableMap<String, ObservableList<TodoItem>> dataContainer = FXCollections.observableHashMap();


    private TodoData() {
        dataContainer.put("Travel", null);
        dataContainer.put("Travel", null);
        dataContainer.put("Work", null);
        dataContainer.put("Study", null);
        dateTimeFormatter = DateTimeFormatter.ofPattern("dd-MM-yy");
    }

    //Add New Todo Item to Item List
    public void addNewTodoItem(TodoItem item) {
        todoItems.add(item);
    }


    //save data to a txt file

    /**
     * This saving method need to be improved
     * @param userName
     */
    public void saveTodoItems(String userName) {
        if(dbInstance.open()) {
            StringBuilder sb = new StringBuilder();
            sb.append("INSERT INTO ");
            sb.append(dbInstance.TODOLIST_TABLE);
            sb.append("(");
            sb.append(dbInstance.TODOLIST_TABLE_USERNAME_COLUMN);
            sb.append(", ");
            sb.append(dbInstance.TODOLIST_TABLE_SHORTDESCRIPTION_COLUMN);
            sb.append(", ");
            sb.append(dbInstance.TODOLIST_TABLE_DETAILS_COLUMN);
            sb.append(", ");
            sb.append(dbInstance.TODOLIST_TABLE_DEADLINE_COLUMN);
            sb.append(") VALUES (?, ?, ?, ?)");
            System.out.println(sb.toString());

            Iterator<TodoItem> itr = todoItems.iterator();

            while(itr.hasNext()) {
                System.out.println("start while to save data");
                TodoItem item = itr.next();
                System.out.println(item.getShortDescription());
                try{
                    Connection conn = dbInstance.getConnection();
                    PreparedStatement prstmnt = conn.prepareStatement(sb.toString());
                    prstmnt.setString(1, userName);
                    prstmnt.setString(2, item.getShortDescription());
                    prstmnt.setString(3, item.getDetails());
                    prstmnt.setString(4, item.getDeadline().format(dateTimeFormatter));
                    prstmnt.executeUpdate();
                    System.out.println("data successfully saved");
                } catch (SQLException e) {
                    System.out.println("failed to save data");
                    e.getMessage();
                    e.printStackTrace();
                }
            }
        }

        dbInstance.close();
    }


    //loading data from the txt file

    public ObservableList<TodoData> loadTodoItems (String userName) throws  NullPointerException {
        if(dbInstance.open()) {
            try(Connection conn = dbInstance.getConnection();
            PreparedStatement prstmnt = conn.prepareStatement("select * from " + dbInstance.TODOLIST_TABLE +
                    " where " + dbInstance.TODOLIST_TABLE_USERNAME_COLUMN + " = '" + userName + "'" );
                ResultSet result = prstmnt.executeQuery()) {
                while(result.next()) {
                    String shortDescription = result.getString(dbInstance.TODOLIST_TABLE_SHORTDESCRIPTION_COLUMN);
                    String details = result.getString(dbInstance.TODOLIST_TABLE_DETAILS_COLUMN);
                    String dateString = result.getString(dbInstance.TODOLIST_TABLE_DEADLINE_COLUMN);
                    LocalDate date = LocalDate.parse(dateString, dateTimeFormatter);
                    TodoItem retrietedTodoItem = new TodoItem(shortDescription, details, date);
                    todoItems.add(retrietedTodoItem);
                }
            } catch (SQLException e) {
                e.getMessage();
            }
        }
      return null;
    }


    public ObservableList<TodoItem> getTodoItems() {
        return todoItems;
    }


    public ObservableMap<String, ObservableList<TodoItem>> getDataContainer() {
        dataContainer.put("Travel", todoItems);
        dataContainer.put("Private", null);
        dataContainer.put("Work", null);
        dataContainer.put("Study", null);
        return dataContainer;
    }

    //Delete Todo Item from the Todo Item list

    //Update Todo Item
}
