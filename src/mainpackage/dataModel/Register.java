package mainpackage.dataModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Register {
    private static Register ourInstance = new Register();
    private static DAOConnection dbInstance = DAOConnection.getInstance();
    private Register() {
    }

    public static Register getInstance() {
        return ourInstance;
    }

    //User Sign up
    public  void createNewUser(User user) {
        String userName = user.getUserName();
        String userMail = user.getEmail();
        if((userMail!= null) && (userName !=null)) {
            System.out.println("Start analysis");
            if((isUserFieldExist(userName))) {
                System.out.println("User Name Already Exist in our db");
                return;
            }else if((isUserFieldExist(userMail))) {
                System.out.println("User Email Already Exist in our db");
                return;
            } else if(!isMail(userMail)) {
                return;
            }else {
                System.out.println("En user creation step");
                //Save user profile to database file
                saveNewUser(new UserAccount(user));
                System.out.println("User succesfully registred");
            }
        } else {

        }
    }


    //check if  user mail or mail exist already exist
    public boolean isUserFieldExist(String field) {
        String sb = querryBuilder(field);
        System.out.print(sb);
        if(dbInstance.open()) {
            try (Connection conn = dbInstance.getConnection();
                 PreparedStatement prStment = conn.prepareStatement(sb.toString());
                 ResultSet resultSet = prStment.executeQuery();) {

                while(resultSet.next()) {
                    if(resultSet.getRow() > 0) {
                        System.out.print(sb.toString());
                        return true;
                    }else {
                        System.out.println("\nAccount does not exist");
                        return false;
                    }
                }
            } catch (Exception e) {
                e.getMessage();
                System.out.println("\nCatch block executed");
                e.getMessage();
                return false;
            }


        }

        dbInstance.close();
        return false;
    }

    private String querryBuilder (String field) {
        StringBuilder sb = new StringBuilder();
        sb.append("select ");
        if(isMail(field)) {
            sb.append(dbInstance.USER_TABLE_EMAIL_COLUMN);
            sb.append(" from ");
            sb.append(dbInstance.USER_TABLE);
            sb.append(" where ");
            sb.append(dbInstance.USER_TABLE_EMAIL_COLUMN);
        } else {
            sb.append(dbInstance.USER_TABLE_USERNAME_COLUMN);
            sb.append(" from ");
            sb.append(dbInstance.USER_TABLE);
            sb.append(" where ");
            sb.append(dbInstance.USER_TABLE_USERNAME_COLUMN);
        }
        sb.append(" = '");
        sb.append(field);
        sb.append("'");
        return sb.toString();
    }



    public boolean isMail(String userName){
        String[] emailSpliter = userName.split("@");
        if(emailSpliter.length == 2 ) {
            System.out.println("successfully splited the email");
            String[] dotSplitter = emailSpliter[1].split("\\.");
            if(dotSplitter.length >= 2) {
                System.out.print("Success find a dot from the second part of the email");
                return true;
            } else {
                return false;
            }
        }
        else {
            return false;
        }
    }

    public void saveNewUser(UserAccount newUserProfile) {
        User user = newUserProfile.getUser();
        StringBuilder sb = new StringBuilder();
        sb.append("INSERT INTO ");
        sb.append(dbInstance.USER_TABLE);
        sb.append("(");
        sb.append(dbInstance.USER_TABLE_FIRSTNAME_COLUMN);
        sb.append(", ");
        sb.append(dbInstance.USER_TABLE_LASTNAME_COLUMN);
        sb.append(", ");
        sb.append(dbInstance.USER_TABLE_USERNAME_COLUMN);
        sb.append(", ");
        sb.append(dbInstance.USER_TABLE_EMAIL_COLUMN);
        sb.append(", ");
        sb.append(dbInstance.USER_TABLE_PASSWORD_COLUMN);
        sb.append(") VALUES (?, ?, ?, ?, ?)");

        if(dbInstance.open()) {
            try(Connection conn = dbInstance.getConnection();
            PreparedStatement prstmt = conn.prepareStatement(sb.toString())) {
                prstmt.setString(1, user.getFirstName());
                prstmt.setString(2, user.getFirstName());
                prstmt.setString(3, user.getUserName());
                prstmt.setString(4, user.getEmail());
                prstmt.setString(5, user.getPassword());
                prstmt.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
                e.getMessage();
            }

        }
        dbInstance.close();
    }

}
