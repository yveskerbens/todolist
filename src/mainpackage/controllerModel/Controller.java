package mainpackage.controllerModel;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.Callback;
import mainpackage.dataModel.Login;
import mainpackage.dataModel.TodoItem;
import mainpackage.dataModel.UserAccount;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public class Controller {

    private static UserAccount userAccount;

    @FXML
    private ListView<String> todoItemContainter;

    @FXML
    ListView<TodoItem> todoListView;

    @FXML
    private BorderPane todoListMainWindow;

    @FXML
    private TextArea itemDetailsTextArea;

    @FXML
    private Label deadLineLabel;

    @FXML
    private Menu profile;

    private TitledPane tilePane;

    private final fdCellData view = new fdCellData();


    public void initialize() {
        userAccount = Login.getInstance().getUserAccount();
        String userName = userAccount.getUser().getUserName();
        System.out.println(userName);
        profile.setText(userName);


        todoItemContainter.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                String item = todoItemContainter.getSelectionModel().getSelectedItem();
                todoListView.setItems(userAccount.getDataCon().get(item));
            }
        });
        todoItemContainter.getItems().addAll(userAccount.getDataCon().keySet());
        todoItemContainter.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        todoItemContainter.getSelectionModel().selectFirst();

        todoItemContainter.setCellFactory(new Callback<ListView<String>, ListCell<String>>() {
            @Override
            public ListCell<String> call(ListView<String> param) {
                ListCell<String>  fdCell = new ListCell<>(){
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if(empty){
                            setGraphic(null);
                        } else {
                            fdCellData cellData = new fdCellData();
                            cellData.setShortDescription(item);
                            cellData.setIcon();
                            setGraphic(cellData.getCellHbox());
                        }
                    }
                };
                return fdCell;
            }
        });

        todoListView.setCellFactory(new Callback<ListView<TodoItem>, ListCell<TodoItem>>() {
            @Override
            public ListCell<TodoItem> call(ListView<TodoItem> param) {
                ListCell<TodoItem> cell = new ListCell<>() {
                    @Override
                    protected void updateItem(TodoItem item, boolean empty) {
                        super.updateItem(item, empty);
                        if(empty) {
                            setGraphic(null);
                        } else {
                            ListCellData cellData = new ListCellData();
                            cellData.setItemInfo(item.getShortDescription(), item.getDetails(), item.getDeadline().toString());
                            cellData.setEditIcon();
                            setGraphic(cellData.getitemCellHbox());
                            if(item.getDeadline().equals(LocalDate.now())) {
                                setTextFill(Color.RED);
                            } else if (item.getDeadline().equals(LocalDate.now().plusDays(1))) {
                                setTextFill(Color.BLUE);
                            }
                        }
                    }
                };
                return cell;
            }
        });
    }

    @FXML
    public void addNewTodoItem() {
        Dialog<ButtonType> dialog = new Dialog<>();
        dialog.initOwner(todoListMainWindow.getScene().getWindow());
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("../viewModel/todoItemDialog.fxml"));
        try{
            dialog.getDialogPane().setContent(fxmlLoader.load());
        } catch (IOException e) {
            e.getMessage();
        }
        dialog.getDialogPane().getButtonTypes().add(ButtonType.OK);
        dialog.getDialogPane().getButtonTypes().add(ButtonType.CANCEL);
        DialogController controller = fxmlLoader.getController();
        Optional<ButtonType> result = dialog.showAndWait();
        if ((result.isPresent() && result.get() == ButtonType.OK)){
           TodoItem item =  controller.processResult();
           todoListView.getSelectionModel().select(item);
        }

    }

    @FXML
    public void showNewItemDialog(){

    }

    @FXML
    public  void saveTodoItem() {
        userAccount.saveUserData();
    }

    @FXML
    public void logoutUser(ActionEvent event) throws  IOException {
        userAccount.saveUserData();
        Parent root = FXMLLoader.load(getClass().getResource("../viewModel/signIn.fxml"));
        Scene signInScene = new Scene(root, 300, 400);
        Stage signInWindow = (Stage)((Node)event.getSource()).getScene().getWindow();
        signInWindow.setTitle("Sign in");
        signInWindow.setScene(signInScene);
        signInWindow.show();
    }
}
