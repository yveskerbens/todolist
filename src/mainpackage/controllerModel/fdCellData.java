package mainpackage.controllerModel;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;


import java.io.IOException;

public class fdCellData {
    @FXML
    private HBox cellHbox;

    @FXML
    private Label  shortDescription;

    @FXML
    private ImageView cellItemIcon;



    public fdCellData() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../viewModel/fdCellView.fxml"));
        fxmlLoader.setController(this);
        try {
            cellHbox = fxmlLoader.load();
        } catch (IOException e) {
            e.getMessage();
            throw new RuntimeException();
        }
    }


    public void setShortDescription (String shd) {
        shortDescription.setText(shd);

    }

    public void setIcon() {
        Image image = new Image(getClass().getResource("../icons/task.png").toExternalForm());
        cellItemIcon.setImage(image);
    }

    public HBox getCellHbox() {
        return cellHbox;
    }
}
