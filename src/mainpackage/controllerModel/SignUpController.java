package mainpackage.controllerModel;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.event.ActionEvent;
import mainpackage.dataModel.Register;
import mainpackage.dataModel.User;


public class SignUpController {
    @FXML
    private GridPane gridPane;
    @FXML
    private TextField firstName;
    @FXML
    private TextField lastName;
    @FXML
    private TextField userName;
    @FXML
    private TextField email;
    @FXML
    private PasswordField password;


    public void initialize() {
        gridPane.setPadding(new Insets(100, 10, 10, 100));
    }

    @FXML
    public void signUp(ActionEvent event){
        try{
            createUser();
            backToSignIn(event);
        } catch(Exception e) {
            e.getMessage();
        }
    }

    private void createUser(){
        String userFirstName = firstName.getText();
        String userLastName = lastName.getText();
        String userUsername = userName.getText().trim();
        String userEmail = email.getText().trim();
        String userPassword = password.getText().trim();
        User newUser = new User(userUsername, userEmail, userFirstName, userLastName, userPassword);
        Register.getInstance().createNewUser(newUser);
        System.out.println("User has been created");
    }

    @FXML
    public void backToSignIn(ActionEvent event) throws  Exception{
        Parent root = FXMLLoader.load(getClass().getResource("../viewModel/signIn.fxml"));
        Scene signInScene = new Scene(root, 300, 400);
        Stage signInWindow = (Stage)((Node)event.getSource()).getScene().getWindow();
        signInWindow.setTitle("Sign in");
        signInWindow.setScene(signInScene);
        signInWindow.show();
    }
}
