package mainpackage.controllerModel;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import java.io.IOException;


public class ListCellData {

    @FXML
    private HBox itemCellHbox;

    @FXML
    private Label shortDescription;

    @FXML
    private TextField details;

    @FXML
    private Label deadline;

    @FXML
    private ImageView editIcon;

    public ListCellData() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../viewModel/listCellView.fxml"));
        fxmlLoader.setController(this);
        try{
            itemCellHbox= fxmlLoader.load();
        } catch(IOException e) {
            throw new RuntimeException();
        }
    }

    public void setItemInfo(String shortD, String dets, String dl) {
        shortDescription.setText(shortD);
        details.setText(dets);
        deadline.setText(dl);

    }

    public void setEditIcon(){
        Image image = new Image(getClass().getResource("../icons/edit.png").toExternalForm());
        editIcon.setImage(image);
    }

    public HBox getitemCellHbox() {
        return itemCellHbox;
    }
}
