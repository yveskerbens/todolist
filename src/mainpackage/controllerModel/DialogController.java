package mainpackage.controllerModel;

import javafx.fxml.FXML;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import mainpackage.dataModel.Login;
import mainpackage.dataModel.TodoItem;
import mainpackage.dataModel.UserAccount;

import java.time.LocalDate;

public class DialogController {
    @FXML
    private TextField shortDescriptionField;

    @FXML
    private TextArea detailsArea;

    @FXML
    private DatePicker deadLinePicker;


    public TodoItem processResult() {
        TodoItem newItem = buildTodoItem (shortDescriptionField.getText(), detailsArea.getText(), deadLinePicker.getValue());
        UserAccount userAccount = Login.getInstance().getUserAccount();
        userAccount.addUserTodoItem(newItem);
        return newItem;
    }

    private TodoItem buildTodoItem (String shortDesc, String details, LocalDate deadline) {
        return new TodoItem(shortDesc, details, deadline);
    }
}
