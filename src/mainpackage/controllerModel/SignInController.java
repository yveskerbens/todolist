package mainpackage.controllerModel;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.scene.Node;
import javafx.event.ActionEvent;
import mainpackage.dataModel.Login;
import mainpackage.dataModel.UserAccount;

import java.io.IOException;

public class SignInController {
    @FXML
    private TextField username;

    @FXML
    private PasswordField password;

    @FXML
    private GridPane gridPane;



    public void initialize(){
        gridPane.setPadding(new Insets(200, 10, 10, 100));

    }

    @FXML
    public void switchToMainWindow(ActionEvent event) throws IOException {
        String user = username.getText(); String passwd = password.getText();
        if(Login.getInstance().isLoginSuccess(user,  passwd)) {
            //Load user data
             UserAccount userAccount = Login.getInstance().getUserAccount();
             try {
                 userAccount.loadUserTodoData();
             } catch (NullPointerException e) {

             }

            //Switch to  user profile interface
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../viewModel/mainwindow.fxml"));
            Parent root = fxmlLoader.load();
            Scene mainAppScene = new Scene(root, 900, 500);
            Stage window = (Stage) ((Node)event.getSource()).getScene().getWindow();
            window.setTitle("Todo List and Tasks Manager");
            window.setScene(mainAppScene);
            window.show();

        } else {

        }
    }

    @FXML
    public  void switchToSignUpWindow(ActionEvent event) throws  IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../viewModel/signUp.fxml"));
        Scene signUpScene = new Scene(root, 300, 400);
        Stage signUpWindow = (Stage)((Node)event.getSource()).getScene().getWindow();
        signUpWindow.setTitle("Sign Up");
        signUpWindow.setScene(signUpScene);
        signUpWindow.show();
    }

}
